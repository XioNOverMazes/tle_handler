#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <vector>
#include <map>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "sgp4sdp4.h"
#include "sat-vis.h"

#define TIME_STEP 15 // Check satellite visibility for each second

typedef std::pair<time_t, time_t> timepair_t;

struct vis_entity_intrvls_t {
	int sat_first;
	int sat_second;
	std::vector<timepair_t> intrvls;
};

struct vis_entity_raw_t {
	int sat_first;
	int sat_second;
	std::vector<time_t> time;
};

char tle_str[3][80];
std::vector<sat_t> sat_vec;
std::vector<vis_entity_raw_t> time_vis_raw;
std::vector<vis_entity_intrvls_t> time_vis_intrvls;

const char *text_init_help =
  "Using : ./TLE_Handler_app --tlefile --tstart --tend"
  "--lat --lon --alt --angle \n												"
	"Raw time input like tstart is time_t						";


void makeIntervals() {
	for (int sat_i = 0; sat_i < time_vis_raw.size(); ++sat_i)
	{
		vis_entity_intrvls_t ent_intrvls;
		ent_intrvls.sat_first = time_vis_raw[sat_i].sat_first;
		ent_intrvls.sat_second = time_vis_raw[sat_i].sat_second;

		int vec_time_siz = time_vis_raw[sat_i].time.size();
		if (vec_time_siz == 1) {
			ent_intrvls.intrvls.push_back(
				timepair_t(time_vis_raw[sat_i].time[0], time_vis_raw[sat_i].time[0]));
				time_vis_intrvls.push_back(ent_intrvls);
				continue;
		}

		for (int i = 0; i < vec_time_siz-1; ) {
			time_t prev_val = time_vis_raw[sat_i].time[i];
			int j = i+1;
			for (; j < vec_time_siz; ++j) {
				// if last sequence 8..9..10
				if (j == vec_time_siz-1 && time_vis_raw[sat_i].time[j]-TIME_STEP == prev_val) {
					ent_intrvls.intrvls.push_back(
						timepair_t(time_vis_raw[sat_i].time[i], time_vis_raw[sat_i].time[j]));
				}
				// end interval if breaks sequence 1..2..3.^.5..
				if (time_vis_raw[sat_i].time[j]-TIME_STEP != prev_val) {
					ent_intrvls.intrvls.push_back(
						timepair_t(time_vis_raw[sat_i].time[i], time_vis_raw[sat_i].time[j-1]));
					break;
				}
				prev_val = time_vis_raw[sat_i].time[j];
			}
			i = j;
			// if last interval solo standing
			if (i == vec_time_siz-1)
				ent_intrvls.intrvls.push_back(
					timepair_t(time_vis_raw[sat_i].time[i], time_vis_raw[sat_i].time[i]));
		}
		time_vis_intrvls.push_back(ent_intrvls);
	}
}

void showIntervals() {
	for (int sat_i = 0; sat_i < time_vis_intrvls.size(); ++sat_i) {
		if (time_vis_intrvls[sat_i].intrvls.size() != 0) {
			std::cout << "For satellite idx " << time_vis_intrvls[sat_i].sat_first;
			std::cout << " and satellite idx " << time_vis_intrvls[sat_i].sat_second;
			std::cout << " visible time intervals are : \n";
			for (int t_i = 0; t_i <  time_vis_intrvls[sat_i].intrvls.size(); ++t_i) {
				std::cout << "t1 : " << time_vis_intrvls[sat_i].intrvls[t_i].first;
				std::cout << " t2 : " << time_vis_intrvls[sat_i].intrvls[t_i].second;
				std::cout << "\n";
			}
		}
	}
}



int main(int argc, char* argv[])
{
	// INIT PARAM
	po::variables_map map_arg_init;
	try {
		po::options_description init_desc("Write --help");
		init_desc.add_options()
			("help"			, text_init_help)
			("tlefile"	, po::value<std::string>(), "Full path to tle file")
			("tstart" 	, po::value<long>(), "Time observing start (takes time_t)")
			("tend"			, po::value<long>(), "Time observing end (takes time_t)")
			("lat" 			, po::value<double>(), "Position latitude")
			("lon"			, po::value<double>(), "Position longitude")
			("alt"			, po::value<double>(), "Position alititude above sea meters")
			("angle"		, po::value<double>(), "Minimal vision angle")
		;
		po::store(po::parse_command_line(argc, argv, init_desc), map_arg_init);
		po::notify(map_arg_init);

		if (map_arg_init.count("help")) {
			std::cout << init_desc << "\n";
		}
	}
	catch(std::exception& e) {
    std::cerr << "error: " << e.what() << "\n";
    return 1;
  }
  catch(...) {
  	std::cerr << "Exception of unknown type!\n";
  }

	// INIT PARAM END

	time_t tstart, tend;
	std::string filepath_tle = map_arg_init["tlefile"].as<std::string>();
	tstart = map_arg_init["tstart"].as<long>();
	tend = map_arg_init["tend"].as<long>();


	qth_t obs_coor;
	obs_coor.lat = map_arg_init["lat"].as<double>(); // latitude
	obs_coor.lon = map_arg_init["lon"].as<double>(); // longitude
	obs_coor.alt = map_arg_init["alt"].as<double>(); // above sea (meters)
	// minimal angle (watch sat-vis func theta variable)
	double min_ang = map_arg_init["angle"].as<double>();


	// READ TLE FILE
	FILE* fp;

	fp = fopen(filepath_tle.c_str(), "r");
	if (fp == NULL)
	{
		printf("Error reading TLE nodes file \n");
		return 1;
	}
	while (true)
	{
		if (fgets(tle_str[0], 80, fp) == NULL)
		{
			printf("Error reading TLE line 1\n");
			fclose(fp);
			break;
		}
		if (fgets(tle_str[1], 80, fp) == NULL)
		{
			printf("Error reading TLE line 2\n");
			fclose(fp);
			return 1;
		}
		if (fgets(tle_str[2], 80, fp) == NULL)
		{
			printf("Error reading TLE line 3\n");
			fclose(fp);
			return 1;
		}
		sat_t temp_sat;
		if (Get_Next_Tle_Set(tle_str, &temp_sat.tle) == 1)
		{
			printf("TEST DATA:\n");
		}
		else
		{
			printf("Could not read TLE data 1 \n");
			return 1;
		}
		printf("%s", tle_str[0]);
		printf("%s", tle_str[1]);
		printf("%s", tle_str[2]);
		select_ephemeris(&temp_sat);
		sat_vec.push_back(temp_sat);
	}
	// READ TLE FILE END

	// For each pair of setellites
	size_t sat_vec_siz = sat_vec.size();
	for (int s1_i = 0; s1_i < sat_vec_siz-1; ++s1_i) {
		for (int s2_i = s1_i+1; s2_i < sat_vec_siz; ++s2_i) {
			vis_entity_raw_t temp_entity;
			temp_entity.sat_first = s1_i;
			temp_entity.sat_second = s2_i;
			// For in time interval
			for (time_t t_i = tstart; t_i < tend; t_i+=TIME_STEP) {
				// time convertion routines
				tm * timeutc = gmtime(&t_i);
				double jul_dat_convt = Julian_Date(timeutc);
				SGP4(&sat_vec[s1_i], jul_dat_convt);
				SGP4(&sat_vec[s2_i], jul_dat_convt);
				// if both visible
				if( (int)get_sat_vis(&sat_vec[s1_i], &obs_coor, jul_dat_convt, min_ang) == 1
				    &&(int)get_sat_vis(&sat_vec[s2_i], &obs_coor, jul_dat_convt, min_ang) == 1) {
					temp_entity.time.push_back(t_i);
				}
			}
			time_vis_raw.push_back(temp_entity);
		}
	}

	makeIntervals();
	showIntervals();

	return 0;
}
